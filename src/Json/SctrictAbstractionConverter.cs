using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Envize.FlexGrid.Json
{
    public class StrictAbstractionConverter<TAbstraction, TImplementation> : JsonConverter<TAbstraction>
        where TImplementation : TAbstraction
    {
        public override TAbstraction Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            return JsonSerializer.Deserialize<TImplementation>(ref reader, JsonPreset.CamelCase);
        }

        public override void Write(Utf8JsonWriter writer, TAbstraction value, JsonSerializerOptions options)
        {
            JsonSerializer.Serialize(writer, value, typeof(TAbstraction), JsonPreset.CamelCase);
        }
    }
}