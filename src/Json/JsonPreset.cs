using System.Text.Json;

namespace Envize.FlexGrid.Json
{
    public static class JsonPreset
    {
        public static JsonSerializerOptions CamelCase = new JsonSerializerOptions()
        {
            PropertyNameCaseInsensitive = true,
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        };
    }
}