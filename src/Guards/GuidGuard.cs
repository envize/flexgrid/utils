using System;
using Envize.FlexGrid.Utils.Validation;

namespace Ardalis.GuardClauses
{
    public static class GuidGuard
    {
        private static GuidValidator _guidValidator { get; } = new GuidValidator();

        public static void InvalidGuid(this IGuardClause guardClause, string guid, string parameterName)
        {
            Guard.Against.NullOrEmpty(guid, parameterName);

            if (!_guidValidator.IsValid(guid))
            {
                throw new ArgumentException($"Guid is invalid '{guid}'.", parameterName);
            }
        }

        public static void InvalidGuid(this IGuardClause guardClause, Guid guid, string parameterName)
        {
            if (guid == Guid.Empty)
            {
                throw new ArgumentException($"Guid is invalid '{guid}'.", parameterName);
            }
        }
    }
}
