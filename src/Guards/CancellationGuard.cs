using System.Threading;

namespace Ardalis.GuardClauses
{
    public static class CancellationGuard
    {
        public static void Cancellation(this IGuardClause guardClause, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                cancellationToken.ThrowIfCancellationRequested();
            }
        }
    }
}
