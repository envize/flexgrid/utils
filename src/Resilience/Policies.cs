using System;
using Polly;

namespace Envize.FlexGrid.Resilience
{
    public static class Policies
    {
        public static Policy RetryUpToTimeout<TException>(TimeSpan timeout, Action<int, TimeSpan> onRetryAction = null)
            where TException : Exception
        {
            var timeoutPolicy = Policy.Timeout(timeout);
            var retryForeverPolicy = Policy.Handle<TException>()
                .WaitAndRetryForever(attempt => 
                {
                    var timeToWait = BackoffFunction.Jitter(attempt);
                    onRetryAction?.Invoke(attempt, timeToWait);
                    return timeToWait;
                });
            var retryUpToTimeoutPolicy = timeoutPolicy.Wrap(retryForeverPolicy);

            return retryUpToTimeoutPolicy;
        }
    }
}