using System;

namespace Envize.FlexGrid.Resilience
{
    public static class BackoffFunction
    {
        private const int DefaultMultiplier = 2;
        private const int DefaultInitialInterval = 300;
        private const int DefaultCap = 30 * 1000;

        private static Random _random = new Random();

        public static TimeSpan Jitter(int attempt)
        {
            var millis = _random.Next(CappedExponential(attempt));
            return TimeSpan.FromMilliseconds(millis);
        }

        private static int Exponential(int attempt,
            int multiplier = DefaultMultiplier,
            int initialInterval = DefaultInitialInterval)
        {
            return (int)(initialInterval * Math.Pow(multiplier, attempt));
        }

        private static int CappedExponential(int attempt,
            int cap = DefaultCap,
            int multiplier = DefaultMultiplier,
            int initialInterval = DefaultInitialInterval)
        {
            return Math.Min(cap, Exponential(attempt, multiplier, initialInterval));
        }
    }
}