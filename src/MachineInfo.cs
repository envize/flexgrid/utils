﻿using System;
using System.Reflection;

namespace Envize.FlexGrid
{
    public static class MachineInfo
    {
        public static string AssemblyName = Assembly.GetEntryAssembly()!.GetName().Name!.ToLower();
        public static string MachineName = Environment.MachineName.ToLower();
        public static string EnvironmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
        public static string ApplicationName = GetApplicationName();
        public static string ApplicationId = $"{ApplicationName}-{MachineName}";

        private static string GetApplicationName()
        {
            var name = Environment.GetEnvironmentVariable("APPLICATION_NAME");
            name ??= AssemblyName;

            return name.ToLower();
        }
    }
}
