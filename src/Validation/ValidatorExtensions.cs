using Envize.FlexGrid.Utils.Validation;
using FluentValidation;

namespace Envize.FlexGrid.Utils
{
    public static class ValidatorExtensions
    {
        private static GuidValidator _guidValidator { get; } = new GuidValidator();
        
        public static IRuleBuilderOptions<T, string> IsGuid<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.Must(input => _guidValidator.IsValid(input));
        }
    }
}
