using System;

namespace Envize.FlexGrid.Utils.Validation
{
    public class GuidValidator
    {
        public bool IsValid(string guid)
        {
            return Guid.TryParse(guid, out _);
        }
    }
}
