using System;
using Xunit;

namespace Ardalis.GuardClauses
{
    public class GuidGuardTests
    {
        [Theory]
        [InlineData("1445ac4a-c177-4b3f-be44-7810552fe6a3")]
        [InlineData("D4403BE4-1A7A-4B0D-8A8C-FA5C923CB219")]
        public void InvalidGuid_ShouldPassGuard_WhenGuidIsValid(string guid)
        {
            // arrange

            // act
            Guard.Against.InvalidGuid(guid, nameof(guid));

            // assert
        }

        [Fact]
        public void InvalidGuid_ShouldThrowArgumentNullException_WhenGuidIsNull()
        {
            // arrange

            // act
            Action act = () => Guard.Against.InvalidGuid(null, "param");

            // assert
            Assert.Throws<ArgumentNullException>(act);
        }

        [Fact]
        public void InvalidGuid_ShouldThrowArgumentException_WhenGuidIsEmpty()
        {
            // arrange

            // act
            Action act = () => Guard.Against.InvalidGuid("", "param");

            // assert
            Assert.Throws<ArgumentException>(act);
        }

        [Theory]
        [InlineData("1445ac4a-c177-4b3f-be44-7810552fe6a")]
        [InlineData("1445ac4a-c177-4b3f-be44-7810552fe6a3a")]
        [InlineData("1445ac4a-c177-4b3f-be44-7810552fe6X3")]
        [InlineData("1445ac4a-c177-4b3f-be44-")]
        [InlineData("1445ac4a")]
        public void InvalidGuid_ShouldThrowArgumentException_WhenGuidIsInvalid(string guid)
        {
            // arrange

            // act
            Action act = () => Guard.Against.InvalidGuid(guid, nameof(guid));

            // assert
            Assert.Throws<ArgumentException>(act);
        }
    }
}