using System;
using System.Threading;
using Xunit;

namespace Ardalis.GuardClauses
{
    public class CancellationGaurdTests
    {
        [Fact]
        public void Cancellation_ShouldPassGuard_WhenNotCancelled()
        {
            // arrange
            var source = new CancellationTokenSource();

            // act
            Guard.Against.Cancellation(source.Token);

            // assert
        }

        [Fact]
        public void Cancellation_ShouldThrowOperationCanceledException_WhenCancelled()
        {
            // arrange
            var source = new CancellationTokenSource();
            source.Cancel();

            // act
            Action act = () => Guard.Against.Cancellation(source.Token);

            // assert
            Assert.Throws<OperationCanceledException>(act);
        }
    }
}